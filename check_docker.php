#!/usr/bin/php

<?php
// Check Docker status via remote API.
// 2018 Dan Ialacci
// returns either an overview of a Docker hosts or status of a specific container
// usage: check_docker.php -h <host name> -m[overview|container] -n <container name>

// 6-26-18 - update to take -n value of container name as index is unreliable. 

define('STATE_OK', 0);
define('STATE_WARNING', 1);
define('STATE_CRITICAL', 2);
define('STATE_UNKNOWN', 3);

$options = getopt("h:m:n:");

$dockerURL = "http://".$options['h'].":2375/containers/json";
$apiData = json_decode(file_get_contents($dockerURL),TRUE);

$containersInfo = array();
$containersDown = 0;
$i = 0;

// get array of all containers present
foreach ($apiData as $container) {

    $containersInfo[$i]['name'] = $container['Names'][0];
    $containersInfo[$i]['id'] = $container['Id'];
    $containersInfo[$i]['state'] = $container['State'];
    $containersInfo[$i]['status'] = $container['Status'];

    if ($container['State'] != "running") {
        $containersDown ++;
    }
    $i++;
};

$containersOnline = count($containersInfo) - $containersDown;

#Return based on mode called
if ($options['m'] == "overview") {
$message = "Showing $containersOnline of ". count($containersInfo) ." containers online. \n";

    if($containersOnline != count($containersInfo)){
        echo "CRITICAL: " . $message;
        exit(STATE_CRITICAL);
    }elseif ($containersOnline == count($containersInfo)) {
        echo "OK: ". $message;
        exit(STATE_OK);
    }else{
	echo "ERROR: ";
	exit(STATE_UNKNOWN);
	}
}

if ($options['m'] == "container" ) {

    if (array_search("/".$options['n'], array_column($containersInfo,'name')) !== false) {

        $index = array_search("/".$options['n'], array_column($containersInfo,'name'));

        $message = "Name: ". trim($containersInfo[$index]['name'], "/") .", State: " . $containersInfo[$index]['state'] . ", Status: " . $containersInfo[$index]['status']. "\n";

        if ($containersInfo[$index]['state'] != "running") {
            echo "CRITICAL: " . $message;
            exit(STATE_CRITICAL);
        }else{
            echo "OK: ". $message;
            exit(STATE_OK);
        }

    }else{
        echo "ERROR: container ". $options['n'] ." not found! \n";
        exit(STATE_CRITICAL);
    }
}
